
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }
  */


var showButton = document.getElementsByClassName('showButton');
var tab = document.getElementsByClassName('tab');

var switcher = false;

showButton[0].onclick = function () {
  if (switcher === false) {
  tab[0].style.visibility = 'visible';
  tab[0].style.opacity = '1';
  tab[0].style.overflow = 'visible';
  switcher = true;
  } else if (switcher === true) {
    close();
  }
}

showButton[1].onclick = function () {
  if (switcher === false) {
  tab[1].style.visibility = 'visible';
  tab[1].style.opacity = '1';
  tab[1].style.overflow = 'visible';
  switcher = true;
  } else if (switcher === true) {
    close();
  }
}

showButton[2].onclick = function () {
  if (switcher === false) {
  tab[2].style.visibility = 'visible';
  tab[2].style.opacity = '1';
  tab[2].style.overflow = 'visible';
  switcher = true;
  } else if (switcher === true) {
    close();
  }
}

function close() {
  tab[0].style.visibility = 'hidden';
  tab[0].style.opacity = '0';
  tab[0].style.overflow = 'hidden';
  tab[1].style.visibility = 'hidden';
  tab[1].style.opacity = '0';
  tab[1].style.overflow = 'hidden';
  tab[2].style.visibility = 'hidden';
  tab[2].style.opacity = '0';
  tab[2].style.overflow = 'hidden';
  switcher = false;
}




console.log(showButton[0]);
  